from rest_framework import serializers
from django.contrib.auth.models import User


class RegistrationSerializer(serializers.ModelSerializer):

	confirm_password = serializers.CharField(style={'input_type': 'password'}, write_only=True)

	class Meta:
		model = User
		fields = ['username', 'password', 'confirm_password']
		extra_kwargs = {
			'password': {'write_only': True}
		}

	def save(self):
		account = User(
				username = self.validated_data['username'],
			)
		password = self.validated_data['password']
		confirm_password = self.validated_data['confirm_password']

		if password != confirm_password:
			raise serializers.ValidationError({'password': 'Passwords must match.'})
		account.set_password(password)
		account.save()

		return account
