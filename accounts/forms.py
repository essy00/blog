from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

class LoginForm(forms.Form):
	username = forms.CharField(max_length=20, label='Kullanıcı Adı')
	password = forms.CharField(max_length=20, label='Parola', widget=forms.PasswordInput)

	def clean(self):
		username = self.cleaned_data.get('username')
		password = self.cleaned_data.get('password')
		if username and password:
			user = authenticate(username=username, password=password)
			if not user:
				raise forms.ValidationError('Kullanıcı adı ya da parola yanlış girildi.')
		return super(LoginForm, self).clean()


class RegisterForm(forms.ModelForm):
	username = forms.CharField(max_length=20, label='Kullanıcı Adı')
	password = forms.CharField(max_length=20, label='Parola', widget=forms.PasswordInput)
	confirm_password = forms.CharField(max_length=20, label='Parola (Tekrar)', widget=forms.PasswordInput)

	class Meta:
		model = User
		fields = [
			'username',
			'password',
			'confirm_password',
		]

	def clean_confirm_password(self):
		password = self.cleaned_data.get('password')
		confirm_password = self.cleaned_data.get('confirm_password')
		if password and confirm_password and password != confirm_password:
			raise forms.ValidationError('Parolalar eşleşmiyor.')
		return confirm_password

	@receiver(post_save, sender=settings.AUTH_USER_MODEL)
	def create_auth_token(sender, instance=None, created=False, **kwargs):
		if created:
			Token.objects.create(user=instance)