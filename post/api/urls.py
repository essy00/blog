from django.urls import path
from post.api.views import (
	
	api_detail_post_view, 
	api_update_post_view,
	api_delete_post_view,
	api_create_post_view,
	ApiBlogListView,

)

app_name = 'post'

urlpatterns = [
	path('<str:slug>/detail/', api_detail_post_view, name='detail'),
	path('<str:slug>/update/', api_update_post_view, name='update'),
	path('<str:slug>/delete/', api_delete_post_view, name='delete'),
	path('create/', api_create_post_view, name='create'),
	path('list/', ApiBlogListView.as_view(), name='list'),
]