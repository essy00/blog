from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.pagination import PageNumberPagination
from rest_framework.generics import ListAPIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.filters import SearchFilter, OrderingFilter

from post.models import Post
from post.api.serializers import PostSerializer


@api_view(['GET',])
@permission_classes((IsAuthenticated,))
def api_detail_post_view(request, slug):
	try:
		post = Post.objects.get(slug=slug)
	except Post.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		serializer = PostSerializer(post)
		return Response(serializer.data)


@api_view(['PUT',])
@permission_classes((IsAuthenticated, IsAdminUser,))
def api_update_post_view(request, slug):
	try:
		post = Post.objects.get(slug=slug)
	except Post.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'PUT':
		serializer = PostSerializer(post, data=request.data)
		data = {}
		if serializer.is_valid():
			serializer.save()
			data['success'] = 'update successful'
			return Response(data=data)
		return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


@api_view(['DELETE',])
@permission_classes((IsAuthenticated, IsAdminUser,))
def api_delete_post_view(request, slug):
	try:
		post = Post.objects.get(slug=slug)
	except Post.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'DELETE':
		operation = post.delete()
		data = {}
		if operation:
			data['success'] = 'delete successful'
		else:
			data['failure'] = 'delete failed'
		return Response(data=data)


@api_view(['POST',])
@permission_classes((IsAuthenticated, IsAdminUser,))
def api_create_post_view(request, slug):
	if request.method == 'POST':
		serializer = PostSerializer(post, data=request.data)
		if serializer.is_valid:
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_404_BAD_REQUEST)


class ApiBlogListView(ListAPIView):
	queryset = Post.objects.all()
	serializer_class = PostSerializer
	authentication_classes = (TokenAuthentication,)
	permission_classes = (IsAuthenticated,)
	pagination_class = PageNumberPagination
	filters_backends = (SearchFilter, OrderingFilter)
	search_fields = ('title', 'text',)
